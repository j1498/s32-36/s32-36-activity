const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");


//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {

    //The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then( result => {

        //the "find" method returns a record if a match is found
        if(result.length > 0) {
            return true

        //No duplicate email found
        //The user is not yet registered in the database
        } else {
            return false
        }
    })
}


//User Registration
module.exports.registerUser = (reqBody) => {

    //Creates a variable "newUser" and instantiates a new User object using the mongoose Model
    //Uses the information from the request body to provide all the necessary information
        let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                //10 is the value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt the password
                //hashSync(<dataToBeHash>,<salt>)
                password: bcrypt.hashSync(reqBody.password, 10)
        })

        //Saves the created object to our database
        return newUser.save().then((user, error) => {

            //User registration failed
            if(error){
                return false

            //User registration successful 
            } else {
                return true
            }
        })
}


//User Authentication
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result =>{

        if(result == null){
            return false
        } else {

            //compareSync(dataToBeCompared, encryptedData)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return { access: auth.createAccessToken(result)}
            } else {
                return false;
            }
        }
    })
}

// Activity

module.exports.getProfile = (data) => {

    return User.findById(data.userId).then(result => {
        result.password = "";
        return result;
    });
};



// S36 Activity

module.exports.enroll = async (data) => {

    if (data.isAdmin == false) {

        let isUserUpdated = await User.findById(data.userId).then(user => {

        user.enrollments.push({courseId: data.courseId});

            return user.save().then((user, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            })
        })

        let isCourseUpdated = await Course.findById(data.courseId).then( course => {
            course.enrollees.push({userId: data.userId});

            return course.save().then((course, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            })
        })

        if (isUserUpdated && isCourseUpdated) {
            return true;
        } else {
            return false;
        }

    } else {
        return `You are not allowed to enroll. For regular users only`
    }
}

