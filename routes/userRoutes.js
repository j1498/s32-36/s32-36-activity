const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for checking if email exists
//Invokes checkEmailExists function from the controller file
//Passes the "body" property of our "request" object to the corresponding controller function
//The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


//Routes for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
//The "auth.verify" will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// Activity

router.post("/enroll", auth.verify, (req, res) => {


	let data = {
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => {
		res.send(resultFromController);
	})
})


router.post("/enroll", auth.verify, (req, res) => {


	let data = {
		courseId: req.body.courseId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.enroll(data).then(resultFromController => {
		res.send(resultFromController);
	})
})


//Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
