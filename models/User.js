const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "Please enter your first name!"]
	},
	lastName: {
		type: String,
		required: [true, "Please enter your last name!"]
	},
	email: {
		type: String,
		required: [true, "Please enter your email!"]
	},
	password: {
		type: String,
		required: [true, "Please enter a password!"]
	},
	isAdmin: {
		type: Boolean,
		default: false,
	},
	mobileNo: {
		type: String,
		required: [true, "Please enter your mobile number!"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Please enter course ID!"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: 'Enrolled'
			}
		}
	]
	
})

module.exports = mongoose.model("User", userSchema);
